---
layout: post
title:  "rafaelhdr freelancer skills"
date:   2018-05-17 08:00:00 -0300
categories: rafaelhdr freelancer
permalink: blog/rafaelhdr-freelancer-skills
lang: en
---
# rafaelhdr freelancer skills

Hello, my name is Rafael and I am a web developer since 2003. If you are here, you might want to hire me as a developer, so I would like to write a little more about my skills.

> In the text, there are links to some open-source projects related.

**My main developer skill is coding for the backend.** I like Node.js and Python languages. You could hire me also hire me for [Golang](https://gitlab.com/shereland/graphql), but I am still a beginner in Go. My experience is developing monolithic applications using Django and also Microservices using [Express](https://gitlab.com/shereland/web), Flask and also [Django](https://github.com/rafaelhdr/portfolio-wall-app).

**I also know frontend.** I have the basic skills developing HTML/CSS/js applications and also develops using React, with experience developing with [single-page application](https://github.com/rafaelhdr/portfolio-wall-app) and also adding components to an [existent application](https://gitlab.com/shereland/web).

**DevOps is something that I really enjoy.** I started learning Linux some years ago (~since 2013) and Docker since 2016. Nowadays, I try always develop my projects using [GitLab CI and Docker Swarm](https://gitlab.com/shereland).

[Check my full portfolio]({% link pages/portfolio.md %})

I already worked in Startups (Dagood, a search engine for Restaurants, Bars and Clubs, and Helpin, for hiring services) and in Innovation area (LARC/Scopus, a laboratory in USP developing and researching for Scopus, which is a Bradesco company partner).

E-mail: [contato@rafaelhdr.com.br]("mailto:contato@rafaelhdr.com.br").

---
layout: post
title:  "Problema no upgrade do Ubuntu"
date:   2014-11-01 12:00:00 -0300
categories: ubuntu upgrade problema
permalink: blog/bugs/problema-no-upgrade-do-ubuntu
lang: pt_br
---
# Problema no upgrade do Ubuntu

Dica rápida:

Estava fazendo upgrade do Ubuntu (da 14.04 para 14.10). De repente, dá o problema abaixo:

![Ubuntu problema update]({{"/assets/posts/2014-11-01-problema-no-upgrade-do-ubuntu/terminal-update.png" | absolute_url }})

Simples. Só clicar em ok.

O problema é que ao clicar nele, e dar enter, não funciona. Aperta pra baixo, e não funciona. Nada está fazendo funcionar.

Basta clicar em cima dele, e então apertar tab.

Confesso que não achei nada óbvio, e fiquei um tempão preocupado, já que estava no final da instalação.

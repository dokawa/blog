---
layout: post
title:  "Como registrar um domínio .com.br"
date:   2014-02-12 18:00:00 -0300
categories: dominio brasil
permalink: blog/about/como-registrar-dominio-com-br
lang: pt_br
---
# Como registrar um domínio .com.br

Para quem não sabe, domínio é o nome do seu site (como por exemplo, o meu domínio é rafaelhdr.com.br).
Registrar um domínio é bem simples, e vai te dar controle sobre ele. Se algum dia você quiser mudar de servidor, você mesmo terá poder para isso.

Para fazer o registro de domínios, utilizaremos o [registro.br](https://registro.br), que é o órgão responsável por domínios .com.br.

## Criação de conta

Se você já tiver conta no registro.br, clique aqui para pular esse passo.

Acesse o registro.br, e então clique à direita, para criar sua própria conta.

![Cadastro no registro.br]({{"/assets/posts/2014-02-12-como-registrar-dominio-com-br/registro-dominio_3.png" | absolute_url }})

Você chegará em uma página semelhante a essa abaixo:

![Formulário de registro de domínio]({{"/assets/posts/2014-02-12-como-registrar-dominio-com-br/registro-dominio-cadastro_1.png" | absolute_url }})

Preencha com seus dados pessoais. O próprio site irá lhe auxiliar com isso. Ao final, você receberá um login. Guarde-o, pois será necessário depois.

## Acesso e criação de domínio

Entre em sua conta. Para isso, basta entrar no registro.br, e então logar preenchendo os campos à direita.

![Área de login de registro de domínio]({{"/assets/posts/2014-02-12-como-registrar-dominio-com-br/registro-dominio-login_1.png" | absolute_url }})

Você chegará em uma página semelhante a essa abaixo (provavelmente sem domínios cadastrados, caso seja a primeira vez).

![Novo domínio]({{"/assets/posts/2014-02-12-como-registrar-dominio-com-br/registro-dominio-dashboard.png" | absolute_url }})

Clique em **Novo Domínio**. Você será redirecionado para a página de novo domínio, como essa abaixo.

![Formulário de novo domínio]({{"/assets/posts/2014-02-12-como-registrar-dominio-com-br/registro-dominio-novo_1.png" | absolute_url }})

Vou explicar os campos para caso de alguma dúvida:

### Domínio

![Domínio]({{"/assets/posts/2014-02-12-como-registrar-dominio-com-br/registro-dominio-tempo.png" | absolute_url }})

Digite o nome do seu domínio (sem o www). No meu caso, foi rafaelhdr.com.br.
Sobre o preço, ele está apenas lhe informando. Na hora do pagamento, você poderá escilher.

### Titular

![Titular]({{"/assets/posts/2014-02-12-como-registrar-dominio-com-br/registro-dominio-titular.png" | absolute_url }})

Você pode escolher entre pessoa física e jurídica. Digite o nome da pessoa que é dona desse domínio. Se for você mesmo, preencha com seus dados. Se for de outra pessoa, preencha com os dados dela.

*Mas se essa pessoa tiver cadastrada no registro.br, você não poderá cadastrar, e ela mesma quem terá que fazer isso.*

### Responsáveis

![Responsáveis]({{"/assets/posts/2014-02-12-como-registrar-dominio-com-br/registro-dominio-responsaveis_1.png" | absolute_url }})

Preencha com login de quem for o responsável (lembra que você viu isso no cadastro?)
Se for você mesmo, preencha com seu login. Se não, peça o login da pessoa que será o responsável.

Em resumo, o técnico quem cuidará dos servidores (pode ser seu desenvolvedor), e de cobrança é aquele que receberá as contas de renovação.

### DNS

![DNS]({{"/assets/posts/2014-02-12-como-registrar-dominio-com-br/registro-dominio-dns.png" | absolute_url }})

Aqui você define os servidores que irão suportar seu site. Se você não tiver ideia do que se trata, clique em Utilizar os servidores DNS do Registro.br. Depois você pega as informações corretas com seu desenvolvedor.

Ao final, basta aceitar os termos de contrato, e clicar em Entrar.
Após esses passos, você receberá e-mails de acompanhamento do que acontece com seu registro. No segundo e-mail, você receberá um link para efetuar o pagamento. Quando você pagar, terá registrado seu próprio domínio com sucesso.

Espero ter ajudado!

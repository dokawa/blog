---
layout: post
title:  "Rafaelhdr renovado"
date:   2014-02-01 06:00:00 -0300
categories: rafaelhdr django
permalink: blog/about/rafaelhdr-site-renovado
lang: pt_br
---
Estou migrando. Antes eu utilizava o Wordpress, e agora estou com o Python Django. Não porque o Wordpress é ruim, mas estou gostando bastante das vantagens que o Python Django pode trazer.

Ainda preciso trazer as notícias do site antigo, mas assim que possível, trago tudo de uma vez, e aproveito para escrever coisas novas.

---
layout: post
title:  "Fim da série de primeiras impressões de Distros"
date:   2015-01-20 15:30:00 -0300
categories: linux distros slackware deepin
permalink: blog/about/fim-da-serie-de-primeiras-impressoes-de-distros
lang: pt_br
---
# Fim da série de primeiras impressões de Distros

Certo. Parei de fazer primeiras impressões de distros. Motivo: azar e correria.

## Azar (desapontamento)

Nos dois últimos testes, eu saí bastante desapontado com os testes. Começou com o [Linux Deepin](http://linuxdeepin.com/index.en.html).

![Linux Deepin]({{"/assets/posts/2015-01-20-fim-da-serie-de-primeiras-impressoes-de-distros/DeepinDesktop4-600x450.png" | absolute_url }})

O desktop é super novo, bonito, bem diferente de todos os outros testes que eu havia feito. Eu realmente estava gostando dessa distribuição. De repente, não liga mais...

Ok, não foi tão de repente. Eu rodei os comandos básicos para atualização (apt-get update e apt-get upgrade), e estava demorando um absurdo pra atualizar. Vi nuns fóruns que podia alterar o servidor, então mudei para um servidor do Brasil (sem nenhum comando maluco, fui pelo próprio modo gráfico), e ao reiniciar, ele inicializa, e ao chegar ao DeepIn fica uma tela preta sem opções.

Eu simplesmente não consegui solução (fui em fóruns, e tentei ver isso, mas não foi mesmo). Achei bem frustrante, e resolvi ir pra próxima.

E acho que falhei na escolha seguinte, em que fui para o [Slackware](http://www.slackware.com/). Imagino que tenha outras distribuições baseadas nela de mais fácil utilização. Pra instalar é por um ambiente gráfico mais simples, e ele pergunta um monte de coisas um tanto mais complicadas. Fui aceitando o default. Ao final, ele me joga em um ambiente de linha de comando.

Graças a conhecimentos em raspberry, consegui o ambiente gráfico (startx), e depois disso travei. A primeira coisa que tentei fazer foi colocar internet, mesmo por cabo. Fiquei um bom tempo sem resultados.

## Correria

Todos nós temos responsabilidades, e com dois testes assim, precisei voltar para um caminho seguro. Voltei para o famoso Ubuntu que tanto gosto.

Por um bom tempo, devo parar com os testes de distros. Com a carga de trabalho aumentando, preciso me resolver por aqui.

E queria deixar os principais motivos de ir para o Ubuntu (e não para os outros testados):

* Já confio na marca e me dou bem com ela - Devo ser um dos poucos que gosta do Unity, mas eu realmente gosto muito;
* Estabilidade com novidades - Algo que eu nem notava, mas o Ubuntu me trazia os últimos kernels, mas que fossem estáveis, o que não acontecia com todos;
* Facilidade - Acho muito mais fácil trabalhar com ele e resolver problemas linux com ele, devido a popularidade;
* Costume - Já estou bem acostumado com os atalhos e com ele próprio.

Espero que tenham gostado dos testes, e num futuro não tão próximo pretendo testar mais outras.

---
layout: post
title:  "rafaelhdr freelancer skills"
date:   2018-05-17 08:00:00 -0300
categories: rafaelhdr freelancer
permalink: blog/rafaelhdr-freelancer-skills
lang: pt_br
---
# rafaelhdr desenvolvedor freelancer

Olá, meu nome é Rafael e sou desenvolvedor web desde 2003. Se você está aqui, talvez esteja interessado em me contratar como desenvolvedor, então gostaria de falar um pouco mais sobre minhas habilidades.

> No texto, estou adicionando alguns links a projetos open-source relacionados.

**Minha principal habilidade é de backend.** Eu gosto das linguagens Node.js e Python. Você poderia me contratar também para desenvolver em [Golang](https://gitlab.com/shereland/graphql), mas ainda sou iniciante em Go. Tenho experiência desenvolvendo aplicações monolíticas com Django e também Microserviços usando [Express](https://gitlab.com/shereland/web), Flask e [Django](https://github.com/rafaelhdr/portfolio-wall-app).

**Também sei frontend.** Possuo as habilidades básicas de HTML/CSS/js e também desenvolvo usando React, tanto para [single-page application (`create-react-app my-app`)](https://github.com/rafaelhdr/portfolio-wall-app) como adicionando componentes em [uma aplicação já existente](https://gitlab.com/shereland/web).

**DevOps é algo que gosto bastante.** Comecei aprendendo Linux alguns anos atrás (~desde 2013) e Docker desde 2016. Hoje em dia, tento sempre desenvolver minhas aplicações utilizando [GitLab CI e Docker Swarm](https://gitlab.com/shereland).

[Veja meu portfolio completo]({% link pages/portfolio-pt_br.md %})

Já trabalhei com Startups (Dagood, buscador de bares, baladas e restaurantes, e Helpin, para contratar prestadores de serviços) e na área de Inovação (LARC/Scopus, um laboratório da USP desenvolvendo e pesquisando para a Scopus, que é uma empresa parceira do Bradesco).

E-mail: [contato@rafaelhdr.com.br]("mailto:contato@rafaelhdr.com.br").

---
layout: post
title:  "4 motivos para fazer seu site responsivo"
date:   2014-03-30 23:00:00 -0300
categories: responsivo
permalink: blog/about/4-motivos-para-fazer-seu-site-responsivo
lang: pt_br
---
# 4 motivos para fazer seu site responsivo

## O que é

Site responsivo é aquele que responde adequadamente à qualquer tipo de tela. Por exemplo, você já viu aqueles sites feitos para Desktop, e quando um usuário o utiliza pelo celular, não está otimizado, causando aquela má impressão para o usuário? Esse é um site não responsivo.
Um site responsivo vai de adaptar aos tamanhos de tela, e será bem visualizável.

Quer um exemplo? Se estiver no desktop, diminua e aumente a janela do seu navegador, e irá perceber a tela se adequando. Você vai perceber que o rafaelhdr é um site responsivo.

## O que se ganha

Um site responsivo pode muito bem acabar dando mais trabalho para os desenvolvedores, e acabar saindo mais caro. Mas as vantagens são muitas:

## Boa navegabilidade

Com um site responsivo, você oferece uma boa navegabilidade para todos os tipos de usuário, o que é importante para que não saiam frustrados de seu site por não conseguir navegar.

## Futuro é mobile

A quantidade de pessoas acessando a internet através de celulares e tablets está crescendo bastante. Se não prestar atenção, será passado para trás.

Veja abaixo um gráfico que mostra esse crescimento entre os anos de 2012 e 2013.

![Exemplo favicon]({{"/assets/posts/2014-03-30-4-motivos-para-fazer-seu-site-responsivo/Mobile-device-share-Ecommerce-stats.jpg" | absolute_url }})

## Facilidade de manusear

É mais fácil fazer a manutenção de sites responsivos. Pois imagine que ao atualizar, você precisa fazer as alterações em dois (ou três) lugares diferentes.

Tenha seu site responsivo, e evite esse retrabalho.

## Indexação do google

Suas páginas ficam menos fragmentadas em um só lugar, e elas tem boa resposta para todos os tipos de usuário. O google vai perceber isso, e suas páginas são mais recomendadas.

É diferente de você possuir diversas páginas com urls diferentes e fragmentadas, onde ainda pode acontecer de usuários terem alta taxa de rejeição.

E pra completar, achei um gráfico cheio de informações referentes ao acesso web por celulares.

![Exemplo favicon]({{"/assets/posts/2014-03-30-4-motivos-para-fazer-seu-site-responsivo/small-graph-responsive.jpeg" | absolute_url }})

[Clique para ver o gráfico completo](http://www.rafaelhdr.com.br/media/uploads/2011-mobile-statistics-stats-facts-infographic-large.jpeg)

*(fonte dos gráficos: [http://www.smartinsights.com/mobile-marketing/mobile-marketing-analytics/mobile-marketing-statistics/](http://www.smartinsights.com/mobile-marketing/mobile-marketing-analytics/mobile-marketing-statistics/))*

---
layout: page
permalink: /
lang: pt_br
---
Meu nome é Rafael. Seja bem vindo ao meu website de portfolio.

## Apenas um blog

Normalmente faço trabalhos como freelancer, mas ultimamente não tenho tempo livre.

Quando possível, procuro postar algum assunto que possa haver interesse.

[Veja meu blog]({% link pages/blog-pt_br.md %})

## Obrigado

Obrigado por visitar meu site.

E-mail: [contato@rafaelhdr.com.br]("mailto:contato@rafaelhdr.com.br")

---
layout: page
title: Sobre
permalink: /about
lang: pt_br
---

Esse é um website portfolio. É chamado de rafaelhdr - nome que uso em minhas redes sociais.

# Um pouco sobre mim

## Passado

Sou desenvolvedor web desde os 15 anos de idade. Naquele tempo, aprendi a desenvolver em PHP, fazendo aplicações locais, subindo com Dreamweaver via FTP e testando em produção.

Na faculdade, aprendi tecnologias novas, aprendendo (e amando) Python, desenvolvendo aplicações web com o framework Django. Também comecei aprendendo sobre Linux, mudando para o Ubuntu.

## Hoje

Eu ainda gosto de Python (bastante), aprendi node.js, trabalho com Java e estou aprendendo Go.

Eu uso Arch Linux para codificar. Inicialmente, foi difícil, no entanto depois de um período e prática consegui me dar bem com ele.

Minha escolha de servidores é o Ubuntu por causa da estabilidade que ele oferece.

Em meu trabalho na Scopus, percebi a importância da automação, e desde então tenho lido bastante sobre DevOps e procuro fazer todos os meus deploys automatizados. Uso Docker (bastante) e GitLab - e GitLab Pipeline - para automação de meus projetos.

[Veja meu portfolio](/pt_br{% link pages/portfolio-pt_br.md %})

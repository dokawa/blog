---
layout: page
permalink: /
lang: en
---
My name is Rafael. Welcome to my portfolio website.

## Just blogging

Normally I do some freelance jobs, but currently not having free time for it.

I have a blot, and I try to post something interesting that might help someone.

[Check the blog posts]({% link pages/blog.md %})

## Thanks

Thanks for coming to visit my website.

E-mail: [contato@rafaelhdr.com.br]("mailto:contato@rafaelhdr.com.br").

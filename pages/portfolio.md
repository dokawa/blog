---
layout: page
title: Portfolio
permalink: /portfolio
lang: en
---
# Portfolio

Below, you can find some of open-source projects that I've contributed to. If you want to, you can follow me at [GitLab](https://gitlab.com/rafaelhdr), [GitHub](https://github.com/rafaelhdr) and [StackOverflow](https://stackoverflow.com/users/1543364/rafael).

## Projects

### Shereland

<span class="portfolio-tag">AWS S3 - Docker - GitLab CI - Golang - Grunt - Javascript - Microservices - MySQL - Node.js - PugJS - Python/Django - React - Redis</span>

Hobby website for sharing books with friends. I wrote about the new Shereland open source at the [blog]({% post_url en/2018-03-11-shereland-open-source %}).

Links: [GitLab](https://gitlab.com/shereland) - [Website](https://www.shereland.com/)

### rafaelhdr

<span class="portfolio-tag">AWS S3 - GitLab CI - Jekyll</span>

My personal portfolio website. Developed with Jekyll and Continuous Deployment with GitLab Pipeline.

Links: [GitLab](https://gitlab.com/rafaelhdr/site)

### Glewlwyd OAuth 2 Server

<span class="portfolio-tag">Docker</span>

This project is an OAuth 2 Server made in C. I've ported the application to Docker.

Links: [GitHub](https://github.com/rafaelhdr/glewlwyd-oauth2-server)

### Wall App

<span class="portfolio-tag">Docker - Python/Django - React</span>

Portfolio web application to practise React and Django.

Links: [GitHub](https://github.com/rafaelhdr/portfolio-wall-app)

### MySQL Docker Backuper

<span class="portfolio-tag">Docker - MySQL - Shell</span>

Image for backup any MySQL database in a Docker Network exporting to AWS S3.

Links: [GitHub](https://github.com/rafaelhdr/mysql-docker-backuper)

### Meteor Google Charts

<span class="portfolio-tag">Javascript - Meteor</span>

Google Charts library I developed for Meteor. +2000 downloads.

Links: [Atmosphere JS](https://atmospherejs.com/rafaelhdr/google-charts) - [GitHub](https://github.com/rafaelhdr/meteor-google-charts)

### Safecar

<span class="portfolio-tag">Python/Django</span>

College project using Django. It was an application that communicates with BeagleBone (simulator of a car).

Links: [GitHub](https://github.com/rafaelhdr/safecar)

### USPGrade

<span class="portfolio-tag">Python/Django</span>

College project using Django. Poll for new ideas for the college.

Links: [GitHub](https://github.com/rafaelhdr/uspgrade)

---
layout: page
title: Portfolio
permalink: /portfolio
lang: pt_br
---
# Portfolio

Abaixo, você pode ver alguns projetos open-source em que contribuí. Se quiser, você pode me seguir em [GitLab](https://gitlab.com/rafaelhdr), [GitHub](https://github.com/rafaelhdr) e [StackOverflow](https://stackoverflow.com/users/1543364/rafael).

## Projetos

### Shereland

<span class="portfolio-tag">AWS S3 - Docker - GitLab CI - Golang - Grunt - Javascript - Microservices - MySQL - Node.js - PugJS - Python/Django - React - Redis</span>

Website para compartilhamento de livros entre amigos. Escrevi sobre o novo Shereland no [blog]({% post_url pt_br/2018-03-11-shereland-open-source %}).

Links: [GitLab](https://gitlab.com/shereland) - [Website](https://www.shereland.com/)

### rafaelhdr

<span class="portfolio-tag">AWS S3 - GitLab CI - Jekyll</span>

Meu website pessoal. Desenvolvi com Jekyll e Continuous Deployment com o GitLab Pipeline.

Links: [GitLab](https://gitlab.com/rafaelhdr/site)

### Glewlwyd OAuth 2 Server

<span class="portfolio-tag">Docker</span>

Esse é um projeto desenvolvido em C. Eu portei deploy com Docker.

Links: [GitHub](https://github.com/rafaelhdr/glewlwyd-oauth2-server)

### Wall App

<span class="portfolio-tag">Docker - Python/Django - React</span>

Aplicação web para praticar React e Django.

Links: [GitHub](https://github.com/rafaelhdr/portfolio-wall-app)

### MySQL Docker Backuper

<span class="portfolio-tag">Docker - MySQL - Shell</span>

Imagem para backup de banco de dados MySQL dentro de uma Rede Docker exportando para AWS S3.

Links: [GitHub](https://github.com/rafaelhdr/mysql-docker-backuper)

### Meteor Google Charts

<span class="portfolio-tag">Javascript - Meteor</span>

Biblioteca de Google Charts que desenvolvi para Meteor. +2000 downloads.

Links: [Atmosphere JS](https://atmospherejs.com/rafaelhdr/google-charts) - [GitHub](https://github.com/rafaelhdr/meteor-google-charts)

### Safecar

<span class="portfolio-tag">Python/Django</span>

Projeto de faculdade usando Django. É uma aplicação que se comunica com um BeagleBone (servindo de simulador para um carro).

Links: [GitHub](https://github.com/rafaelhdr/safecar)

### USPGrade

<span class="portfolio-tag">Python/Django</span>

Projeto de faculdade usando Django. Votação de novas ideias para a faculdade.

Links: [GitHub](https://github.com/rafaelhdr/uspgrade)

---
layout: page
title: Tools
permalink: /tools/
lang: en
---
# Web tools

This area is for simple tools to be used online.  When people ask me something that I can share here, I add it here.

The tools I have for now:

* [HTML Generator]({% link pages/html-generator.html %})
* [Slugify Online]({% link pages/slugify-online.html %})

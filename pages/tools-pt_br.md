---
layout: page
title: Ferramentas
permalink: /tools/
lang: pt_br
---
# Ferramentas web

Essa área é para ferramentas online. Quando pessoas pedem algo simples que posso compartilhar, coloco aqui.

Ferramentas criadas:

* [Gerador HTML](/pt_br{% link pages/html-generator-pt_br.html %})
* [Slugify Online](/pt_br{% link pages/slugify-online-pt_br.html %})

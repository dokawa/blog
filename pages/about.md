---
layout: page
title: About
permalink: /about
lang: en
---

This is my portfolio website. It is called rafaelhdr - the name that I use on my social networks.

# A little about me

## Past

I have been web developer since the age of 15. I learned how to develop using PHP, developing applications locally, uploading with Dreamweaver via FTP and testing on production.

At college, I learned other technologies, such as Python, developing web applications with Django framework. I learned about Linux specific focused on Ubuntu.

## Today

I still use python (a lot), learned node.js, also using Java and learning Go.

I use Arch Linux for coding. Initially, it was hard to use at first, however, after learning about it and practicing, I was able to get the hang of it.

My choice of servers is Ubuntu because of the stability that it offers.

At my workspace in Scopus, I realized the importance of automation, since then I have been reading about DevOps and I try to make all my deployments automated. I use Docker (a lot) and GitLab - and GitLab Pipeline - for my projects automation.

[Check my portfolio]({% link pages/portfolio.md %})
